﻿using UnityEngine;
using System.Collections;

public class CharacterTapper : MonoBehaviour {

	private Camera camera;

	// Use this for initialization
	void Start () {
		//Gets a reference to the camera
		camera = Camera.main;
	}
	
	// Update is called once per frame
	void Update () {

		//Check if the user has tapped the screen 
		if(Input.GetMouseButtonDown(0))
		{
			//may a ray that emits from where the screen was tapped
			//this ray goes into the game world
			//Ray ray = camera.ScreenPointToRay(Input.GetTouch(0).position);
			Ray ray = camera.ScreenPointToRay(Input.mousePosition);

			RaycastHit hit;

			//If this ray hit anything, this if statement returns true
			if(Physics.Raycast(ray, out hit))
			{
				//check if what we hit with the ray is the samurai
				if(hit.collider.tag == "samurai")
				{
					Debug.Log("Hit samurai");
					//Tell the samurai to run it's "Hit" method
					hit.collider.SendMessage("Hit");
				}
			}
		}
	}
}
