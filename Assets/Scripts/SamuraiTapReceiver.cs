﻿using UnityEngine;
using System.Collections;

public class SamuraiTapReceiver : MonoBehaviour {

	Animation animation;
	private int animIndex;
	private int maxAnimIndex;

	// Use this for initialization
	void Start () {
		animation = GetComponent<Animation>();
		animIndex = 0;
		maxAnimIndex = animation.GetClipCount();
	}
	

	public void Hit()
	{
		animIndex++;

		if(animIndex > maxAnimIndex)
		{
			animIndex = 0;
		}

		animation.clip = GetClipByIndex(animIndex);
		animation.Play();
	}

	AnimationClip GetClipByIndex(int index)
	{
		int i = 0;
		foreach (AnimationState animationState in animation)
		{
			if (i == index)
				return animationState.clip;
			i++;
		}
		return null;
	}
}
